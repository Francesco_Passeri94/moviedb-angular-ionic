import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { LoginPageModule } from '../pages/login/login.module';
import { Dettaglio } from '../pages/dettaglio/dettaglio';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MovieDBserviceProvider } from '../providers/movie-d-bservice/movie-d-bservice';
import { HttpModule } from '@angular/http';

//Modulo per storage
import { IonicStorageModule } from '@ionic/storage';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { AuthService } from '../service/Auth-service';
import { PreferitiPage } from '../pages/preferiti/preferiti';
import { PaginaTestPage } from '../pages/pagina-test/pagina-test';
import { LogicaCordovaPage } from '../pages/logica-cordova/logica-cordova';
import { Network } from '@ionic-native/network';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    //LoginPageModule,
    Dettaglio,
    PreferitiPage,
    PaginaTestPage,
    LogicaCordovaPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),     
    LoginPageModule,
    HttpModule,
    //Import per storage
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    Dettaglio,
    PreferitiPage,
    PaginaTestPage,
    LogicaCordovaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MovieDBserviceProvider,
    AuthServiceProvider,
    AuthService,
    Network
  ]
})
export class AppModule {}
