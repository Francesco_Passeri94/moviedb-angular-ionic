import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { PreferitiPage } from '../pages/preferiti/preferiti';
import { LoginPage } from '../pages/login/login';
import {AuthService} from '../service/Auth-service';
import { Storage } from '@ionic/storage';

import { Subscription } from 'rxjs';
import { AlertController } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  logged : Boolean = false;
  subscription: Subscription;
  preferiti = new Array;

  constructor(public platform: Platform, 
              public statusBar: StatusBar, 
              public splashScreen: SplashScreen, 
              public modalCtrl: ModalController,
              private AuthService: AuthService,
              public storage: Storage,
              public alertCtrl: AlertController
             ) {
    this.initializeApp();

    this.subscription = this.AuthService.subject.subscribe(loggato => { this.logged = loggato; console.log(this.logged); });
    
    //Per usare il servizio storage di Ionic bisogna inserire queste righe nel costruttore di ogni componente in cui viene usato
    this.storage = new Storage({
      name : 'MovieDBApp',
      storeName : 'user',
      driverOrder : ['localstorage']
    });
    
    this.storage.get('loggato').then((result) => {
      console.log('Nuovo accesso', result);
      if(result==true){
        this.logged=true;
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openHome() {
    this.nav.setRoot(HomePage);
  }

  openLoginModal() {
    const modal = this.modalCtrl.create("LoginPage", {});
    modal.present();
  }

  showFavorite(){
    this.storage.get('Preferiti').then((result) => {
      console.log('Array', result);
      this.preferiti=result;

      if(this.preferiti && (this.preferiti.length>0)){
        /*this.nav.push(PreferitiPage, 
          this.preferiti
        );*/
        this.nav.push(PreferitiPage);
      }
      else {
        const alert = this.alertCtrl.create({
          title: 'Errore',
          subTitle: 'La tua lista dei preferiti è vuota',
        });
        alert.present();
      }
    });
  }

  Logout(){
    this.AuthService.Logout();
  }
}
