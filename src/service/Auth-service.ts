import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()


export class AuthService {

    email : String;
    password : String;

    loggato = false;

    //preferiti = new Array;

    public subject = new Subject<any>()

    constructor(public storage: Storage) { 
        this.storage = new Storage({
            name : 'MovieDBApp',
            storeName : 'user',
            driverOrder : ['localstorage']
        });

        /*this.storage.get('Preferiti').then((result) => {
            if(result != undefined){
                this.preferiti=result;
            }
        });*/
    } 

    signIn(email,password){
        this.email=email;
        this.password=password;
        this.loggato=true;
        this.storage.set('loggato',this.loggato);
        this.storage.set('Email',this.email);
        this.storage.set('Password',this.password);
    }

    Logout(){
        this.loggato=false;
        //this.storage.remove('loggato');
        this.storage.set('loggato',this.loggato);
        //this.preferiti=[];
        this.storage.remove('Email');
        this.storage.remove('Password');
        this.storage.remove('Preferiti');
        this.subject.next(false);
    }

}