import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

@Injectable()


export class MovieDBService {

  constructor(private http: Http) { 

  } 

  getResponseMovie(){
    return this.http
    .get("https://api.themoviedb.org/3/movie/popular?api_key=c4d79d0d1e50bf8bc86b7afbd240e4df")
    .map(res => res.json());      
  }

}