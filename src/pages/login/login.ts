import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import {AuthService} from '../../service/Auth-service';

@IonicPage()

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  email : String;
  password : String;

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private authService: AuthService) {

  }

  validateUser():boolean{
    return this.email=="Io@eng.com" && this.password=="password";
  }

  signIn(){
    if(this.validateUser()){
      console.log("LOGIN EFFETTUATO");
      this.authService.signIn(this.email,this.password);
      this.authService.subject.next(true);
      this.navCtrl.pop();
    }else{
      this.showAlert("Wrong email or password");
    }
  }

  showAlert(err) {
    let alert = this.alertCtrl.create({
      title: "Error!",
      subTitle: err,
      buttons: ["OK"]
    });
    alert.present();
  }

  close(){
    this.navCtrl.pop();        
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
