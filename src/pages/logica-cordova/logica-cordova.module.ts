import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogicaCordovaPage } from './logica-cordova';

@NgModule({
  declarations: [
    LogicaCordovaPage,
  ],
  imports: [
    IonicPageModule.forChild(LogicaCordovaPage),
  ],
})
export class LogicaCordovaPageModule {}
