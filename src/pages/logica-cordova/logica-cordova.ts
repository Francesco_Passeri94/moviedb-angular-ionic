import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { Platform } from 'ionic-angular';

/**
 * Generated class for the LogicaCordovaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-logica-cordova',
  templateUrl: 'logica-cordova.html',
})
export class LogicaCordovaPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private network: Network,
              private platform: Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LogicaCordovaPage');

    //Così dovrei usare il plugin network per vedere la connessione appena entrato nella pagina

    this.platform.ready().then(() => {
      // Now all cordova plugins are ready!
      // Network probabilmente funziona solo in un  device reale
      //Nell'oggetto Network ho tutto quello che riguarda la mia connessione
      console.log(this.network);
    });
  
  }

}
