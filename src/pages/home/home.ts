import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Dettaglio } from '../dettaglio/dettaglio';
import { PaginaTestPage } from '../pagina-test/pagina-test';
import { LogicaCordovaPage } from '../logica-cordova/logica-cordova';
import {MovieDBService} from '../../service/MovieDB-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [MovieDBService]
})
export class HomePage {

  popularFilmList : any;
  

  constructor(public navCtrl: NavController, private MovieService: MovieDBService,public navParams: NavParams) {
   
  }

  getResponseMovieDB(){
    this.MovieService.getResponseMovie().subscribe(result => { 
      this.popularFilmList = result.results;
      console.log(this.popularFilmList);
    });
  }

  ionViewDidLoad() {
    this.getResponseMovieDB();
  }

  gotoDetails(film){
    console.log("Entrato Dettaglio");
    this.navCtrl.push(Dettaglio, {
      film
    });
  }

  gotopaginaTest(){
    this.navCtrl.push(PaginaTestPage);
  }

  gotoLogica(){
    this.navCtrl.push(LogicaCordovaPage);
  }

}
