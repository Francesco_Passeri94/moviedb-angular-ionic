import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

//import {AuthService} from '../../service/Auth-service';
/**
 * Generated class for the DettaglioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'dettaglio',
  templateUrl: 'dettaglio.html'
})
export class Dettaglio {

  film : any;
  loggato : Boolean;

  //se dichiaro name qui la variabile mi verrà sempre sovrascritta (anche se era piena) a ogni nuova accesso nella pagina di dettaglio
  name='star-outline';

 

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage/*,private authService : AuthService*/) {
    this.storage = new Storage({
      name : 'MovieDBApp',
      storeName : 'user',
      driverOrder : ['localstorage']
    });
    
    this.film =this.navParams.get('film');
    console.log(this.film);
    this.storage.get('loggato').then((result) => {
      console.log('loggato dettaglio', result);
      this.loggato=result;
    });
    console.log('Loggato dettaglio' + this.loggato);
    /*Usare un ciclo for per controllare se il nome del relativo film è nell'array preferiti (l'array sarebbe meglio se lo prendo
    dal localstorage senno viene sovrascritto al riavvio dell'app se prendo quello del service anche se sono ancora loggato) e 
    semmai mettere il relativo name a star */
    var i = 0;
  
    this.storage.get('Preferiti').then((result) => {
      console.log('Array Dettaglio', result);
      var preferiti = new Array;
      preferiti=result;
      if(preferiti){
        for(i;i<preferiti.length;i++){
          if(preferiti[i].title === this.film.title){
            this.name='star';
          }      
        }
      }
    });

  }

  addFavorite(){
    if(this.name==='star-outline'){
      this.name='star';
      /*this.authService.preferiti.push(this.film);
      console.log(this.authService.preferiti);
      this.storage.set('Preferiti',this.authService.preferiti);
      console.log(this.name);*/

      this.storage.get('Preferiti').then((result) => {
        var preferiti = new Array;
        if(result != undefined){
            preferiti=result;
            preferiti.push(this.film);
            this.storage.set('Preferiti',preferiti);
        }
        else
        {
          preferiti[0]=this.film;
          this.storage.set('Preferiti',preferiti);
        }
    });
    }
    else if(this.name==='star') {
      this.name='star-outline';
     
      var i=0;
      this.storage.get('Preferiti').then((result) => {
        console.log('Array Dettaglio', result);
        var preferiti = new Array;
        preferiti=result;
        if(preferiti!=undefined){
          for(i;i<preferiti.length;i++){
            if(preferiti[i].title === this.film.title){
              preferiti.splice(i,1);
            }      
          }
          this.storage.set('Preferiti',preferiti);
        }
      });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DettaglioPage');
  }

}
