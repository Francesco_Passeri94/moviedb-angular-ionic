import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Dettaglio } from './dettaglio';

@NgModule({
  declarations: [
    Dettaglio,
  ],
  imports: [
    IonicPageModule.forChild(Dettaglio),
  ],
})
export class DettaglioPageModule {}
