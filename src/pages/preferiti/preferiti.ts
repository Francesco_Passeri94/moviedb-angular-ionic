import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Dettaglio } from '../dettaglio/dettaglio';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the PreferitiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-preferiti',
  templateUrl: 'preferiti.html',
})
export class PreferitiPage {

  preferiti : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
    this.storage = new Storage({
      name : 'MovieDBApp',
      storeName : 'user',
      driverOrder : ['localstorage']
    });

    //this.getFavoriteFilm();
  }

  getFavoriteFilm(){
    //this.preferiti =this.navParams.data;  
    this.storage.get('Preferiti').then((result) => {
      this.preferiti=result;
    });
    console.log('Preferiti in pagina' + this.preferiti)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreferitiPage');
  }

  ionViewWillEnter(){
    this.getFavoriteFilm();    
  }

  gotoDetails(film){
    console.log("Entrato Dettaglio");
    this.navCtrl.push(Dettaglio, {
      film
    });
  }

}
