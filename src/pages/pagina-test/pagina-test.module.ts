import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaginaTestPage } from './pagina-test';

@NgModule({
  declarations: [
    PaginaTestPage,
  ],
  imports: [
    IonicPageModule.forChild(PaginaTestPage),
  ],
})
export class PaginaTestPageModule {}
